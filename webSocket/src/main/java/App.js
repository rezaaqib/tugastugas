const connection = new WebSocket("ws://localhost:1313");

connection.onopen = function () {
    console.log("connect to server");
    doSend = ({
        type: "trigger"
    })
};

var doSend = (message) => {
    connection.send(message)
}

document.getElementById("btnSave").onclick = function () {
    var nik = document.getElementById("nik").value;
    var nama = document.getElementById("nama").value;
    var tempat_lahir = document.getElementById("tempat_lahir").value;
    var tanggal_lahir = document.getElementById("tanggal_lahir").value;
    var jenis_kelamin = document.getElementById("jenis_kelamin").value;
    var alamat = document.getElementById("alamat").value;
    var agama = document.getElementById("agama").value;
    var status_perkawinan = document.getElementById("status_perkawinan").value;
    var pekerjaan = document.getElementById("pekerjaan").value;
    var kewarganegaraan = document.getElementById("kewarganegaraan").value;
    var rt = document.getElementById("rt").value;
    var rw = document.getElementById("rw").value;
    var kelurahan = document.getElementById("kelurahan").value;
    var kecamatan = document.getElementById("kecamatan").value;
    var golongan_darah = document.getElementById("golongan_darah").value;
    connection.send(
        JSON.stringify({
            type: "data",
            nik: nik,
            nama: nama,
            tempat_lahir: tempat_lahir,
            tanggal_lahir: tanggal_lahir,
            jenis_kelamin: jenis_kelamin,
            alamat: alamat,
            agama: agama,
            status_perkawinan: status_perkawinan,
            pekerjaan: pekerjaan,
            kewarganegaraan: kewarganegaraan,
            rt: rt,
            rw: rw,
            kelurahan: kelurahan,
            kecamatan: kecamatan,
            golongan_darah: golongan_darah
        })
    );
};