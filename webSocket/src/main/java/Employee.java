import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;

public class Employee {
    Connection connection = new ConnDB().connect();
    ArrayList<String> datas = new ArrayList<>();

    public void query(String theJSON) throws SQLException {
        JSONObject theResponse = new JSONObject(theJSON);

        if (theResponse.getString("type").equalsIgnoreCase("data")) {
            String nik = theResponse.getString("nik");
            String nama = theResponse.getString("nama");
            String tempat_lahir = theResponse.getString("tempat_lahir");
            String tanggal_lahir = theResponse.getString("tanggal_lahir");
            String jenis_kelamin = theResponse.getString("jenis_kelamin");
            String alamat = theResponse.getString("alamat");
            String agama = theResponse.getString("agama");
            String status_perkawinan = theResponse.getString("status_perkawinan");
            String pekerjaan = theResponse.getString("pekerjaan");
            String kewarganegaraan = theResponse.getString("kewarganegaraan");
            String rt = theResponse.getString("rt");
            String rw = theResponse.getString("rw");
            String kelurahan = theResponse.getString("kelurahan");
            String kecamatan = theResponse.getString("kecamatan");
            String golongan_darah = theResponse.getString("golongan_darah");

            inputData(nik, nama, tempat_lahir, tanggal_lahir, jenis_kelamin, alamat, agama, status_perkawinan,
                    pekerjaan, kewarganegaraan, rt, rw, golongan_darah, kecamatan, kelurahan);
        }
    }

    public void inputData(String nik, String nama, String tempat_lahir, String tanggal_lahir, String jenis_kelamin,
                          String alamat, String agama, String status_perkawinan, String pekerjaan, String kewarganegaraan, String rt,
                          String rw, String kelurahan, String kecamatan, String golongan_darah) throws SQLException {
        String query = "INSERT INTO `karyawanweb`.`data` (`nik`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `agama`, `status_perkawinan`, `pekerjaan`, `kewarganegaraan`, rt, rw, kelurahan, kecamatan, golongan_darah) VALUES ('"
                + nik + "', '" + nama + "', '" + tempat_lahir + "', '" + tanggal_lahir + "', '" + jenis_kelamin + "', '"
                + alamat + "', '" + agama + "', '" + status_perkawinan + "', '" + pekerjaan + "', '" + kewarganegaraan
                + "', '" + rt + "', '" + rw + "', '" + kelurahan + "', '" + kecamatan + "', '" + golongan_darah + "')";
        PreparedStatement pstm = connection.prepareStatement(query);
        pstm.executeUpdate(query);
    }

    public void showData() throws SQLException {
        String query = "SELECT * FROM `data`";
        PreparedStatement pstm = connection.prepareStatement(query);
        ResultSet res = pstm.executeQuery(query);

        while (res.next()) {
            String nik = res.getString("nik");
            String nama = res.getString("nama");
            String tempat_lahir = res.getString("tempat_lahir");
            String tanggal_lahir = res.getString("tanggal_lahir");
            String jenis_kelamin = res.getString("jenis_kelamin");
            String alamat = res.getString("alamat");
            String agama = res.getString("agama");
            String status_perkawinan = res.getString("status_perkawinan");
            String pekerjaan = res.getString("pekerjaan");
            String kewarganegaraan = res.getString("kewarganegaraan");
            String rt = res.getString("rt");
            String rw = res.getString("rw");
            String kelurahan = res.getString("kelurahan");
            String kecamatan = res.getString("kecamatan");
            String golongan_darah = res.getString("golongan_darah");

            String response = "{\"nik\": \"" + nik + "\", \"nama\": \"" + nama + "\", \"tempat_lahir\": \""
                    + tempat_lahir + "\", \"tanggal_lahir\": \"" + tanggal_lahir + "\", \"jenis_kelamin\": \""
                    + jenis_kelamin + "\", \"alamat\" : \"" + alamat + "\", \"agama\": \"" + agama
                    + "\", \"status_perkawinan\": \"" + status_perkawinan + "\", \"pekerjaan\": \"" + pekerjaan
                    + "\", \"kewarganegaraan\": \"" + kewarganegaraan + "\", \"rt\": \"" + rt + "\", \"rw\": \"" + rw
                    + "\", \"kelurahan\": \"" + kelurahan + "\", \"kecamatan\": \"" + kecamatan
                    + "\", \"golongan_darah\": \"" + golongan_darah + "\"}";

            datas.add(response);
        }
    }

    ArrayList<String> getDatas() throws Exception {
        showData();
        return datas;
    }

}