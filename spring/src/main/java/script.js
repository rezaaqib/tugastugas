function addData() {

    let nik = document.getElementById("nik").value;
    let name = document.getElementById("name").value;
    let tempat_lahir = document.getElementById("tempat_lahir").value;
    let tanggal_lahir = document.getElementById("tanggal_lahir").value;
    let jenis_kelamin = document.getElementById("jenis_kelamin").value;
    let agama = document.getElementById("agama").value;
    let alamat = document.getElementById("alamat").value;
    let rtrw = document.getElementById("rtrw").value;
    let kelurahan = document.getElementById("kelurahan").value;
    let kecamatan = document.getElementById("kecamatan").value;
    let golongan_darah = document.getElementById("golongan_darah").value;
    let status_perkawinan = document.getElementById("status_perkawinan").value;
    let pekerjaan = document.getElementById("pekerjaan").value;
    let kewarganegaraan = document.getElementById("kewarganegaraan").value;

    let jsonStr = JSON.stringify({
        nik: nik,
        name: name,
        tempat_lahir: tempat_lahir,
        tanggal_lahir: tanggal_lahir,
        jenis_kelamin: jenis_kelamin,
        golongan_darah: golongan_darah,
        alamat: alamat,
        rtrw: rtrw,
        kelurahan: kelurahan,
        kecamatan: kecamatan,
        pekerjaan: pekerjaan,
        agama: agama,
        status_perkawinan: status_perkawinan,
        kewarganegaraan: kewarganegaraan
    });

    fetch('http://localhost:8888/addKaryawan', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(data => {
        
            console.log('Success:', data);
            window.location.reload()
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}
function deleteData(id) {
    let del = confirm("yakin ingin menghapus data?")
    if(del) {
        fetch(`http://localhost:8888/delete/${id}`, {
            method: 'DELETE',
        })
            .then(response => location.reload())
            .catch(console.error)
    }
}



    function updateData() {
        let id = document.getElementById("id").value;
        let nik = document.getElementById("edit-nik").value;
        let name = document.getElementById("edit-name").value;
        let tempat_lahir = document.getElementById("edit-tempat_lahir").value;
        let tanggal_lahir = document.getElementById("edit-tanggal_lahir").value;
        let jenis_kelamin = document.getElementById("edit-jenis_kelamin").value;
        let agama = document.getElementById("edit-agama").value;
        let alamat = document.getElementById("edit-alamat").value;
        let rtrw = document.getElementById("edit-rtrw").value;
        let kelurahan = document.getElementById("edit-kelurahan").value;
        let kecamatan = document.getElementById("edit-kecamatan").value;
        let golongan_darah = document.getElementById("edit-golongan_darah").value;
        let status_perkawinan = document.getElementById("edit-status_perkawinan").value;
        let pekerjaan = document.getElementById("edit-pekerjaan").value;
        let kewarganegaraan = document.getElementById("edit-kewarganegaraan").value;

        let jsonStr = JSON.stringify({
            id: id,
            nik: nik,
            name: name,
            tempat_lahir: tempat_lahir,
            tanggal_lahir: tanggal_lahir,
            jenis_kelamin: jenis_kelamin,
            golongan_darah: golongan_darah,
            alamat: alamat,
            rtrw: rtrw,
            kelurahan: kelurahan,
            kecamatan: kecamatan,
            pekerjaan: pekerjaan,
            agama: agama,
            status_perkawinan: status_perkawinan,
            kewarganegaraan: kewarganegaraan
        });


        fetch('http://localhost:8888/update', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: jsonStr,
        })
            .then(response => response.json())
            .then(() => {
                console.log(jsonStr)
            })
            .catch((error) => {
                console.error('Error:', error);
            });
}
function getData(url) {
    let http = new XMLHttpRequest();
    http.open("GET", url, true)
    http.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let response = JSON.parse(this.responseText)
            console.log(response.length)

            let msgReceipt = ""
            let no = 1;

            for (let j = 0; j < response.length; j++) {
                console.log(response[j])
                msgReceipt += `
                <tr>
                    <td>${no}</td>
                    <td>${response[j].nik}</td>
                    <td>${response[j].name}</td>
                    <td>${response[j].tempat_lahir}</td>
                    <td>${response[j].tanggal_lahir}</td>
                    <td>${response[j].golongan_darah}</td>
                    <td>${response[j].alamat}</td>
                          <td>
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${response[j].name}Detail">
                Detail
            </button>

            <div class="modal fade" id="${response[j].name}Detail" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModelLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" >DETAIL DATA EMPLOYEE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <form style="font-weight: bold">
                                <div class="form-group">
                                    <label for="nik">NIK</label>
                                    
                                    <br>
                                    ${response[j].nik}
                                </div>
                                <div class="form-group">
                                    <label for="name">NAMA</label>
                                     <br>
                                    ${response[j].name}
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir">TEMPAT LAHIR</label>
                                     <br>
                                    ${response[j].tempat_lahir}
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir">PEKERJAAN</label>
                                     <br>
                                    ${response[j].pekerjaan}
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">TANGGAL LAHIR</label>
                                     <br>
                                    ${response[j].tanggal_lahir}
                                </div>

                                <div class="form-group">
                                    <label for="jenis_kelamin">JENIS KELAMIN</label>
                                     <br>
                                    ${response[j].jenis_kelamin}
                                </div>

                                <div class="form-group">
                                    <label for="golongan_darah">GOLONGAN DARAH</label>
                                     <br>
                                    ${response[j].golongan_darah}
                                </div>
                                <div class="form-group">
                                    <label for="alamat">ALAMAT</label>
                                     <br>
                                    ${response[j].alamat}
                                </div>
                                <div class="form-group">
                                    <label for="rtrw">RT/RW</label>
                                     <br>
                                    ${response[j].rtrw}
                                </div>
                                <div class="form-group">
                                    <label for="kelurahan">KELURAHAN</label>
                                     <br>
                                    ${response[j].kelurahan}
                                </div>
                                <div class="form-group">
                                    <label for="kecamatan">KECAMATAN</label>
                                     <br>
                                    ${response[j].kecamatan}
                                </div>
                                <div class="form-group">
                                    <label for="agama">AGAMA</label>
                                     <br>
                                    ${response[j].agama}
                                </div>
                                <div class="form-group">
                                <label for="status_perkawinan">STATUS PERKAWINAN</label>
                                     <br>
                                    ${response[j].status_perkawinan}
                                </div>

                                <div class="form-group">
                                    <label for="kewarganegaraan">KEWARGANEGARAAN</label>
                                     <br>
                                    ${response[j].kewarganegaraan}
                                </div>

                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                        Close
                                    </button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <!-- <button class="btn btn-primary" onclick="return btnAdd()">Save Data</button> -->
                                    <input type="button" class="btn btn-primary" value="Save Data" onclick="addData()">
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
            
            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="deleteData(`+ response[j].id +`)">
                Delete
            </button>
            
             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${response[j].name}update">
                Update
            </button>
            
             <div class="modal fade" id="${response[j].name}update" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel" >ADD DATA EMPLOYEE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
<input type="hidden" id="id" value="${response[j].id}">
                            <form style="font-weight: bold">
                                <div class="form-group">
                                    <label for="edit-nik">NIK</label>
                                    <input type="text" class="form-control" id="edit-nik" autofocus required value="${response[j].nik}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-name">NAMA</label>
                                    <input type="text" class="form-control" id="edit-name" required value="${response[j].name}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-tempat_lahir">TEMPAT LAHIR</label>
                                    <input type="text" class="form-control" id="edit-tempat_lahir" required value="${response[j].tempat_lahir}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-tempat_lahir">PEKERJAAN</label>
                                    <input type="text" class="form-control" id="edit-pekerjaan" required value="${response[j].pekerjaan}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-tanggal_lahir">TANGGAL LAHIR</label>
                                    <input type="date" class="form-control" id="edit-tanggal_lahir" required value="${response[j].tanggal_lahir}">
                                </div>

                                <div class="form-group">
                                    <label for="edit-jenis_kelamin">JENIS KELAMIN</label>
                                    <select class="custom-select" id="edit-jenis_kelamin" required>
                                        <option value="${response[j].jenis_kelamin}">${response[j].jenis_kelamin}</option>
                                        <option value="LAKI-LAKI">Laki-Laki</option>
                                        <option value="PEREMPUAN">Perempuan</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="edit-golongan_darah">GOLONGAN DARAH</label>
                                    <select class="custom-select" id="edit-golongan_darah" required>
                                        <option value="${response[j].golongan_darah}">${response[j].golongan_darah}</option>
                                        <option value="A" required>A</option>
                                        <option value="B" required>B</option>
                                        <option value="AB" required>AB</option>
                                        <option value="O" required>O</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="edit-alamat">ALAMAT</label>
                                    <input type="text" class="form-control" id="edit-alamat" required value="${response[j].alamat}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-rtrw">RT/RW</label>
                                    <input type="text" class="form-control" id="edit-rtrw" required value="${response[j].rtrw}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-kelurahan">KELURAHAN</label>
                                    <input type="text" class="form-control" id="edit-kelurahan" required value="${response[j].kelurahan}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-kecamatan">KECAMATAN</label>
                                    <input type="text" class="form-control" id="edit-kecamatan" required value="${response[j].kecamatan}">
                                </div>
                                <div class="form-group">
                                    <label for="edit-agama">AGAMA</label>
                                    <select class="custom-select" id="edit-agama" required>
                                        <option value="${response[j].agama}">${response[j].agama}</option>
                                        <option value="ISLAM" required>ISLAM</option>
                                        <option value="PROTESTAN" required>PROTESTAN</option>
                                        <option value="KATOLIK" required>KATOLIK</option>
                                        <option value="HINDU" required>HINDU</option>
                                        <option value="BUDDHA" required>BUDDHA</option>
                                        <option value="KHONGHUCU" required>KHONGHUCU</option>
                                    </select>
                                </div>
                                <label for="edit-status_perkawinan">STATUS PERKAWINAN</label>
                                <div class="form-group">
                                    <select class="custom-select" id="edit-status_perkawinan" required>
                                        <option value="${response[j].status_perkawinan}">${response[j].status_perkawinan}</option>
                                        <option value="BELUM MENIKAH" required>BELUM MENIKAH</option>
                                        <option value="SUDAH MENIKAH" required>SUDAH MENIKAH</option>
                                        <option value="SUDAH PERNAH MENIKAH" required>SUDAH PERNAH MENIKAH
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="edit-kewarganegaraan">KEWARGANEGARAAN</label>
                                    <select class="custom-select" id="edit-kewarganegaraan" required>S
                                        <option value="${response[j].kewarganegaraan}">${response[j].kewarganegaraan}</option>
                                        <option value="WNI">WNI</option>
                                        <option value="WNA">WNA</option>
                                    </select>
                                </div>

                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                        Close
                                    </button>                                 
                                    <button type="submit" class="btn btn-primary" onclick="updateData()">UPDATE</button>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>

                </td>
                </tr>
                `
                no++;
            }
            document.getElementById("tableData").innerHTML = msgReceipt
        }
    }
    http.send()
}

getData("http://localhost:8888/karyawans")