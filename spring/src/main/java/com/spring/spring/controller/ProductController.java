package com.spring.spring.controller;

import com.spring.spring.entity.Product;
import com.spring.spring.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class  ProductController {
    @Autowired
    private ProductService service;

    @PostMapping("/addKaryawan")
    public Product addKaryawan(@RequestBody Product product) {
        return service.saveProduct(product);
    }

    @PostMapping("/addKaryawans")
    public List<Product> addKaryawans(@RequestBody List<Product> products) {
        return service.saveProducts(products);
    }

    @GetMapping("/karyawans")
    public List<Product> findAllProducts() {
        return service.getProducts();
    }

    @GetMapping("/karyawanById/{id}")
    public Product findProductById(@PathVariable int id) {
        return service.getProductById(id);
    }

    @GetMapping("/karyawanByName/{name}")
    public Product findByName(@PathVariable String name) {
        return service.getProductByName(name);
    }

    @PutMapping("/update")
    public Product updateProduct(@RequestBody Product product) {
        return service.updateProduct(product);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteProductById(@PathVariable int id) {
        return service.deleteProduct(id);
    }
}
